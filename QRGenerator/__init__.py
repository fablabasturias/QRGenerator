from flask import Flask
from flask import render_template
from flask import request


import qrcode
import PIL

app = Flask(__name__)

@app.route('/')
def hello(name=None):
    return render_template('index.html', name=name)


@app.route('/generate', methods=['POST', 'GET'])
def generate():
    texto = None
    if request.method == 'POST':
        if (request.form['texto']):
            texto = request.form['texto']

            qr = qrcode.QRCode(
                    version=4,
                    error_correction=qrcode.constants.ERROR_CORRECT_H,
                    box_size=10,
                    border=4,
                    )

            qr.add_data(texto)
            qr.make(fit=True)

            qrimg = qr.make_image()
            qr_w, qr_h = qrimg.size

            labimg = PIL.Image.open("static/qrlab.png")
            lab_w, lab_h = labimg.size

            offset = ((qr_w - lab_w) / 2, ((qr_h - lab_h) / 2)-5)

            qrimg.paste(labimg, offset)

            qrimg = qrimg.resize((qr_w*5, qr_h*5), PIL.Image.NEAREST)
            qrimg.save("static/out.png", "PNG")

        return render_template('qr.html', texto=texto)

if __name__ == '__main__':
    app.run(debug=True)


